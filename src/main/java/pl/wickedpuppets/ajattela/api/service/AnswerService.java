package pl.wickedpuppets.ajattela.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wickedpuppets.ajattela.api.model.Answer;
import pl.wickedpuppets.ajattela.api.repository.AnswerRepository;

@Service
public class AnswerService {

    private AnswerRepository answerRepository;

    @Autowired
    public AnswerService(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    public void add(Answer answer) {
        answerRepository.save(answer);
    }


}
