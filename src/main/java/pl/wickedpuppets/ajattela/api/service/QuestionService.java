package pl.wickedpuppets.ajattela.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wickedpuppets.ajattela.api.model.Question;
import pl.wickedpuppets.ajattela.api.repository.QuestionRepository;

@Service
public class QuestionService {

    private QuestionRepository questionRepository;

    @Autowired
    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public void add(Question question) {
        questionRepository.save(question);
    }

}
