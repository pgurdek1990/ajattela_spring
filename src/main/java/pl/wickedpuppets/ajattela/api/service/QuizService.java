package pl.wickedpuppets.ajattela.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wickedpuppets.ajattela.api.model.Quiz;
import pl.wickedpuppets.ajattela.api.repository.QuizRepository;

import java.util.List;
import java.util.Optional;

@Service
public class QuizService {
    private QuizRepository quizRepository;

    @Autowired
    public QuizService(QuizRepository quizRepository) {
        this.quizRepository = quizRepository;
    }

    public void add(Quiz quiz) {
        quizRepository.save(quiz);
    }

    public List<Quiz> getAll() {
        return quizRepository.findAll();
    }

    public Quiz getById(Long id) {
        return quizRepository.getOne(id);
    }

}
