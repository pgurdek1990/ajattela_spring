package pl.wickedpuppets.ajattela.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.wickedpuppets.ajattela.api.model.Question;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
}
