package pl.wickedpuppets.ajattela.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Answer {

    @GeneratedValue
    @Id
    private Long id;

    private String name;
    private String beaconId;

    public Answer() {
    }

    public Answer(String name, String beaconId) {
        this.name = name;
        this.beaconId = beaconId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }
}
