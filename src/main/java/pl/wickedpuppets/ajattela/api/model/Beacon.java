package pl.wickedpuppets.ajattela.api.model;

public class Beacon {

    private String beaconId;
    private User user;

    public Beacon() {
    }

    public Beacon(String beaconId, String userName) {
        this.beaconId = beaconId;
        this.user = new User(userName);
    }

    public String getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Beacon beacon = (Beacon) o;

        if (beaconId != null ? !beaconId.equals(beacon.beaconId) : beacon.beaconId != null) return false;
        return user != null ? user.equals(beacon.user) : beacon.user == null;
    }

    @Override
    public int hashCode() {
        int result = beaconId != null ? beaconId.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }
}
