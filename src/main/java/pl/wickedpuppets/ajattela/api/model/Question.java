package pl.wickedpuppets.ajattela.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Question {

    @GeneratedValue
    @Id
    private Long id;

    private String name;

    @OneToMany
    private List<Answer> answers = new ArrayList<>();

    private String goodAnswerBeaconId;

    private String url;


    public Question() {

    }

    public Question(String name, List<Answer> answers, String goodAnswerBeaconId, String url) {
        this.name = name;
        this.answers = answers;
        this.goodAnswerBeaconId = goodAnswerBeaconId;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public String getGoodAnswerBeaconId() {
        return goodAnswerBeaconId;
    }

    public void setGoodAnswerBeaconId(String goodAnswerBeaconId) {
        this.goodAnswerBeaconId = goodAnswerBeaconId;
    }

    public Answer getGood() {
        for (Answer answer : answers) {
            if (answer.getBeaconId().equals(goodAnswerBeaconId)) return answer;
        }
        return null;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
