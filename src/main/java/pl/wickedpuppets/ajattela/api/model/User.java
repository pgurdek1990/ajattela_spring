package pl.wickedpuppets.ajattela.api.model;

public class User implements Comparable<User>{

    private String name;
    private Integer points = 0;

    public User(){

    }

    public User(String name) {
        this.name = name;
        this.points = 0;
    }

    public User(String name, Integer points) {
        this.name = name;
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return name != null ? name.equals(user.name) : user.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    public void addPoint() {
        this.points += 1;
    }

    @Override
    public int compareTo(User o) {
        int comparePoints = ((User) o).getPoints();

        //ascending order
        return comparePoints - this.points;
    }
}
