package pl.wickedpuppets.ajattela.api.controllers;


import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.wickedpuppets.ajattela.api.model.Answer;
import pl.wickedpuppets.ajattela.api.model.Beacon;
import pl.wickedpuppets.ajattela.api.model.User;

import java.util.*;

@Controller
@RequestMapping("/users")
public class UserController {

    public Set<User> users;
    public Map<User, Map<Beacon, Integer>> users2;
    public Map<Beacon, Integer> beacons;

    public UserController() {
        this.users = new HashSet<>();
        this.beacons = new HashMap<>();
        this.users2 = new HashMap<>();
    }
//
//    @PostMapping("/add2")
//    @ResponseStatus(value = HttpStatus.OK)
//    void add2(@RequestBody User user) {
//        Map<Beacon, Integer> map = new HashMap<>();
//        users2.putIfAbsent(user, map);
//    }

    @PostMapping("/add")
    @ResponseStatus(value = HttpStatus.OK)
    void add(@RequestBody User user) {
        System.out.println(user.getName());
        System.out.println(user.getPoints());
        System.out.println(users.add(user));
    }

    @PostMapping("/beacons")
    @ResponseStatus(value = HttpStatus.OK)
    void getAnswer(@RequestBody Beacon beacon) {
        beacons.putIfAbsent(beacon, 0);
        beacons.put(beacon, beacons.get(beacon) + 1);
        for (Beacon bea : beacons.keySet()) {
            System.out.println(bea.getUser().getName() + "=" + bea.getBeaconId());
            System.out.println(beacons.get(bea));
        }
    }

//    @PostMapping("/beacons2")
//    @ResponseStatus(value = HttpStatus.OK)
//    void getAnswer2(@RequestBody Beacon beacon) {
//        System.out.println(beacon.getUser().getName());
//        users2.get(beacon.getUser()).putIfAbsent(beacon, 0);
//        users2.get(beacon.getUser()).put(beacon, beacons.get(beacon) + 1);
//
//    }

//    @PostMapping("/check")
//    @ResponseStatus(value = HttpStatus.OK)
    void checkAnswer(Answer good) {
        for (User user : users) {
            Map<String, Integer> map = new HashMap<>();
            for (Beacon beacon : beacons.keySet()) {
                if (beacon.getUser().equals(user)) {
                    System.out.println("uzytkownik");
                    map.put(beacon.getBeaconId(), beacons.get(beacon));
                }
            }
            if(map.size() > 0) {
            String userAnswer = findMax(map);
            if(good.getBeaconId().equals(userAnswer)) {
                for (User userInList :  users) {
                    if(userInList.equals(user)) {
                        userInList.addPoint();
                        System.out.println(userInList.getPoints() + user.getName());
                        break;
                    }
                }
            }
            }

        }
        //resetBeacons();

        System.out.println(good.getBeaconId());
    }

    public void resetBeacons() {
        this.beacons = new HashMap<>();
    }

    private String findMax(Map<String, Integer> map) {
        Map.Entry<String, Integer> maxEntry = null;
            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                    maxEntry = entry;
                }
            }
            return maxEntry.getKey();
    }

    public Set<User> getUsers() {
        return users;
    }

    public Map<Beacon, Integer> getBeacons() {
        return beacons;
    }

    public List<User> getTopUsers() {
        System.out.println(users);
        ArrayList<User> sortedUsers = new ArrayList<>(users);
        Collections.sort(sortedUsers);
        return sortedUsers;
    }
}
