package pl.wickedpuppets.ajattela.api.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.wickedpuppets.ajattela.api.model.User;

@Controller
@RequestMapping("/")
public class MainController {

    @GetMapping("")
    String index(Model model){
        User user = new User("Pati",50);
        System.out.println("wbilem");
        model.addAttribute("user", user);
        return "index"; //nazwa templatki
    }
}
