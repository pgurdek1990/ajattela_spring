package pl.wickedpuppets.ajattela.api.controllers;

import org.apache.catalina.core.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.wickedpuppets.ajattela.api.model.Answer;
import pl.wickedpuppets.ajattela.api.model.Quiz;
import pl.wickedpuppets.ajattela.api.service.QuizService;

@Controller
@RequestMapping("/quizzes")
public class QuizController {


    private QuizService quizService;
    public Long actualQuizId;

    @Autowired
    ConfigurableApplicationContext context;

    @Autowired
    public QuizController(QuizService quizService) {
        this.quizService = quizService;
    }

    @GetMapping("")
    String getAll(Model model) {
        model.addAttribute("quizzes", quizService.getAll());
        return "quizzes";
    }


    @GetMapping("/{id}")
    String begin(Model model, @PathVariable Long id) {
        setActualQuizId(id);
        model.addAttribute("quiz", quizService.getById(id));
        return "begin";

    }

    @GetMapping("/{id}/{question}")
    String begin(Model model, @PathVariable Long id, @PathVariable int question) {
        context.getBean(UserController.class).resetBeacons();
        setActualQuizId(id);
        Quiz quiz = quizService.getById(id);
        if(question < quiz.getQuestions().size()) {
            model.addAttribute("quiz", quiz);
            model.addAttribute("answers", quiz.getQuestions().get(question));
            model.addAttribute("question", question);
            return "question";
        }
        else return "redirect:/quizzes/" + quiz.getId().toString() + "/end";
    }

    @GetMapping("/{id}/{question}/answer")
    String answer(Model model, @PathVariable Long id, @PathVariable int question) {
        setActualQuizId(id);
        Quiz quiz = quizService.getById(id);

        if(question < quiz.getQuestions().size()) {
            Answer good = quiz.getQuestions().get(question).getGood();
            model.addAttribute("quiz", quiz);
            model.addAttribute("good", good);
            model.addAttribute("question", question);
            model.addAttribute("users", context.getBean(UserController.class).getTopUsers());
            evaluate(good);
            return "answer";
        }
        else return "redirect:/quizzes/" + quiz.getId().toString() + "/end";
    }

    @GetMapping("/{id}/end")
    String end(Model model, @PathVariable Long id){
        setActualQuizId(id);
        model.addAttribute("quiz", quizService.getById(id));
        model.addAttribute("users", context.getBean(UserController.class).getTopUsers());
        if(context.getBean(UserController.class).getTopUsers().size() == 0) return "redirect:/quizzes";
        return "end";
    }

    private void evaluate(Answer good) {
        context.getBean(UserController.class).checkAnswer(good);
    }

    public Long getActualQuizId() {
        return actualQuizId;
    }

    public void setActualQuizId(Long actualQuizId) {
        if (actualQuizId == null)
        this.actualQuizId = actualQuizId;
    }
}
