package pl.wickedpuppets.ajattela.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.wickedpuppets.ajattela.api.model.Answer;
import pl.wickedpuppets.ajattela.api.model.Question;
import pl.wickedpuppets.ajattela.api.model.Quiz;
import pl.wickedpuppets.ajattela.api.service.AnswerService;
import pl.wickedpuppets.ajattela.api.service.QuestionService;
import pl.wickedpuppets.ajattela.api.service.QuizService;

import java.util.Arrays;
import java.util.List;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private AnswerService answerService;
    private QuestionService questionService;
    private QuizService quizService;

    @Autowired
    public DevBootstrap(AnswerService answerService, QuestionService questionService, QuizService quizService) {
        this.answerService = answerService;
        this.questionService = questionService;
        this.quizService = quizService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        this.onInit();
    }

    private void onInit() {
        Answer a1 = new Answer("cat", "Do1l");
        Answer a2 = new Answer("dog", "yEKi");
        Answer a3 = new Answer("mouse", "VEpV");
        answerService.add(a1);
        answerService.add(a2);
        answerService.add(a3);
        List<Answer> l1 = Arrays.asList(a1, a2, a3);
        Question q1 = new Question("Which animal makes miau", l1, "Do1l", "");

        questionService.add(q1);

        Answer a11 = new Answer("cat", "Do1l");
        Answer a22 = new Answer("dog", "yEKi");
        Answer a33 = new Answer("mouse", "VEpV");
        answerService.add(a11);
        answerService.add(a22);
        answerService.add(a33);
        List<Answer> l11 = Arrays.asList(a11, a22, a33);
        Question q11 = new Question("Which animal make miau", l11, "Do1l", "https://s3-eu-west-1.amazonaws.com/zippi.bucket.artist/minnimals/What-do-Animals-say----Blue-12027/original_web_0nly.jpg?AWSAccessKeyId=AKIAI2AGUGFA7DVDZM5Q");
        questionService.add(q11);


        Answer b1 = new Answer("x2 + y2", "Do1l");
        Answer b2 = new Answer("x2 + xy + y2", "yEKi");
        Answer b3 = new Answer("x2 + 2xy + y2", "VEpV");
        List<Answer> l2 = Arrays.asList(b1, b2, b3);
        answerService.add(b1);
        answerService.add(b2);
        answerService.add(b3);
        Question q2 = new Question("(x + y)2 = ?? ", l2, "VEpV", "");
        questionService.add(q2);

        Answer b11 = new Answer("x2 + y2", "Do1l");
        Answer b22 = new Answer("x2 + xy + y2", "yEKi");
        Answer b33 = new Answer("x2 + 2xy + y2", "VEpV");
        List<Answer> l22 = Arrays.asList(b11, b22, b33);
        answerService.add(b11);
        answerService.add(b22);
        answerService.add(b33);
        Question q22 = new Question("(x + y)2 = ?? ", l22, "VEpV", "https://www.wikihow.com/images/thumb/7/78/Add-and-Subtract-Integers-Step-9-Version-3.jpg/aid278360-v4-728px-Add-and-Subtract-Integers-Step-9-Version-3.jpg");
        questionService.add(q22);

        Answer c11 = new Answer("rock and roll", "Do1l");
        Answer c22 = new Answer("sedimentary rock", "yEKi");
        Answer c33 = new Answer("pet rock", "VEpV");
        List<Answer> k22 = Arrays.asList(c11, c22, c33);
        answerService.add(c11);
        answerService.add(c22);
        answerService.add(c33);
        Question q23 = new Question("What is the strongest force on earth?", k22, "yEKi", "");
        questionService.add(q23);

        Answer d11 = new Answer("ice", "Do1l");
        Answer d22 = new Answer("storm", "yEKi");
        Answer d33 = new Answer("rain", "VEpV");
        List<Answer> n22 = Arrays.asList(d11, d22, d33);
        answerService.add(d11);
        answerService.add(d22);
        answerService.add(d33);
        Question q24 = new Question("Explain what hard water is", n22, "Do1l", "");
        questionService.add(q24);

        Answer e11 = new Answer("Taxonomy", "Do1l");
        Answer e22 = new Answer("Originology", "yEKi");
        Answer e33 = new Answer("Racism", "VEpV");
        List<Answer> m22 = Arrays.asList(e11, e22, e33);
        answerService.add(e11);
        answerService.add(e22);
        answerService.add(e33);
        Question q25 = new Question("What do we call the science of classifying living things?", m22, "Do1l", "");
        questionService.add(q25);

        Quiz quiz1 = new Quiz("Test Quiz", Arrays.asList(q1, q2, q23, q24, q25), "revision");
        Quiz quiz2 = new Quiz("Testos Quizos", Arrays.asList(q11, q22), "learning");

        quizService.add(quiz1);
        quizService.add(quiz2);
    }
}
