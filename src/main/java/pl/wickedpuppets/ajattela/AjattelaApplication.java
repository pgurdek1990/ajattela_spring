package pl.wickedpuppets.ajattela;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AjattelaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AjattelaApplication.class, args);
	}
}
